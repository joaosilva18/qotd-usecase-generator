apiVersion: v1
kind: ConfigMap
metadata:
  name: qotd-usecase-library
  namespace: qotd-load
data:
  rating_cpu.json: >
    {
        "id": "rating_cpu",
        "name": "Rating CPU",
        "description": "Increase CPU to 6",
        "steps": [
            {
                "service": "ratings",
                "name": "Increase cpu usage",
                "type": "cpu",
                "value": 6
            }
        ]
    }

  rating_erroneous_calls_log.json: >
    {
        "id": "rating_erroneous_calls_logs",
        "name": "Ratings service errenous calls on GET /ratings/:id (with logs)",
        "description": "Rating service failing with 500/404 errors half of the time (with logs).",
        "steps": [
            {
                "service": "ratings",
                "name": "Rating service failing with 500/404 errors half of the time (with logs).",
                "type": "setCallDistributionOverride",
                "endpoint": "GET /ratings/:id",
                "options": [
                    {
                        "code": 0,
                        "weight": 2
                    },
                    {
                        "code": 500,
                        "weight": 1,
                        "payload": "Uh oh",
                        "template": "Unexpected internal error: Bobo is having a moment."
                    },
                    {
                        "code": 404,
                        "weight": 1,
                        "payload": "huh?",
                        "template": "Unexpected internal error: Bobo has no idea what you are talking about."
                    }
                ]
            }
        ]
    }
  rating_erroneous_calls: >
    {
        "id": "rating_erroneous_calls",
        "name": "Ratings service errenous calls on GET /ratings/:id (no logs).",
        "description": "Rating service failing with 500/404 errors half of the time (no logs).",
        "steps": [
            {
                "service": "ratings",
                "name": "Rating service failing with 500/404 errors half of the time (no logs).",
                "type": "setCallDistributionOverride",
                "endpoint": "GET /ratings/:id",
                "options": [
                    {
                        "code": 0,
                        "weight": 2
                    },
                    {
                        "code": 500,
                        "weight": 1,
                        "payload": "Uh oh"
                    },
                    {
                        "code": 404,
                        "weight": 1,
                        "payload": "huh?"
                    }
                ]
            }
        ]
    }

  rating_latency.json: >
    {
        "id": "rating_latency",
        "name": "Rating Latency",
        "description": "Increase latency of GET /ratings/:id to 1 second",
        "steps": [
            {
                "service": "ratings",
                "type": "latency",
                "name": "Increase latency in primary GET /ratings/:id to 1 second",
                "endpoint": "GET /ratings/:id",
                "value": {
                    "mean": 1000,
                    "stdev": 200,
                    "min": 500,
                    "max": 5000
                }
            }
        ]
    }

  rating_memory.json: >
    {
        "id": "rating_memory",
        "name": "Rating Memory",
        "description": "Increase memory to 6",
        "steps": [
            {
                "service": "ratings",
                "name": "Increase memory usage",
                "type": "memory",
                "value": 6
            }
        ]
    }

  rating_unknown_error.json: >
    {
        "id": "rating_unknown_error",
        "name": "Rating Log Unknown Error",
        "description": "Generate a new log, with obvious error keywords.",
        "steps": [
            {
                "service": "ratings",
                "name": "Unknown Error log.",
                "type": "setLogger",
                "options": {
                    "name": "Nasty Error log",
                    "template": "ERROR oh error WARNING nasty problem status 500 problem, nasty problem !!! ",
                    "fields": { },
                    "repeat": {
                        "mean": 1000,
                        "stdev": 500,
                        "min": 500,
                        "max": 3000
                    }
                }
            }
        ]
    }

  rating_unknown_normal.json: >
    {
        "id": "rating_unknown_normal",
        "name": "Rating Log Unknown Normal",
        "description": "Generate a new log, with no obvious error keywords.",
        "steps": [
            {
                "service": "ratings",
                "name": "Unknown Normal log.",
                "type": "setLogger",
                "options": {
                    "name": "Kind and typical new log",
                    "template": "La de dah.  Nothing important here.  Move right along.",
                    "fields": { },
                    "repeat": {
                        "mean": 1000,
                        "stdev": 500,
                        "min": 500,
                        "max": 3000
                    }
                }
            }
        ]
    }

  logs_logs_logs.json: >
    {
        "id": "logs_logs_logs",
        "name": "Log Unknown Error in each service",
        "description": "Creates a new log message with error indicators in the text in all services.",
        "steps": [
            {
                "service": "ratings",
                "name": "Unknown Error log.",
                "type": "setLogger",
                "options": {
                    "name": "Nasty Error log",
                    "template": "Ratings service - ERROR oh error WARNING nasty problem status 500 problem.",
                    "fields": { },
                    "repeat": {
                        "mean": 1000,
                        "stdev": 500,
                        "min": 500,
                        "max": 3000
                    }
                }
            },
            {
                "service": "author",
                "name": "Unknown Error log.",
                "type": "setLogger",
                "options": {
                    "name": "Nasty Error log",
                    "template": "Author service - ERROR oh error WARNING nasty problem status 500 problem.",
                    "fields": { },
                    "repeat": {
                        "mean": 1000,
                        "stdev": 500,
                        "min": 500,
                        "max": 3000
                    }
                }
            },
            {
                "service": "engraving",
                "name": "Unknown Error log.",
                "type": "setLogger",
                "options": {
                    "name": "Nasty Error log",
                    "template": "Engraving service - ERROR oh error WARNING nasty problem status 500 problem.",
                    "fields": { },
                    "repeat": {
                        "mean": 1000,
                        "stdev": 500,
                        "min": 500,
                        "max": 3000
                    }
                }
            },
            {
                "service": "image",
                "name": "Unknown Error log.",
                "type": "setLogger",
                "options": {
                    "name": "Nasty Error log",
                    "template": "Image service - ERROR oh error WARNING nasty problem status 500 problem.",
                    "fields": { },
                    "repeat": {
                        "mean": 1000,
                        "stdev": 500,
                        "min": 500,
                        "max": 3000
                    }
                }
            },
            {
                "service": "pdf",
                "name": "Unknown Error log.",
                "type": "setLogger",
                "options": {
                    "name": "Nasty Error log",
                    "template": "PDF service - ERROR oh error WARNING nasty problem status 500 problem.",
                    "fields": { },
                    "repeat": {
                        "mean": 1000,
                        "stdev": 500,
                        "min": 500,
                        "max": 3000
                    }
                }
            },
            {
                "service": "quote",
                "name": "Unknown Error log.",
                "type": "setLogger",
                "options": {
                    "name": "Nasty Error log",
                    "template": "Quote service - ERROR oh error WARNING nasty problem status 500 problem.",
                    "fields": { },
                    "repeat": {
                        "mean": 1000,
                        "stdev": 500,
                        "min": 500,
                        "max": 3000
                    }
                }
            },
            {
                "service": "web",
                "name": "Unknown Error log.",
                "type": "setLogger",
                "options": {
                    "name": "Nasty Error log",
                    "template": "Web service - ERROR oh error WARNING nasty problem status 500 problem.",
                    "fields": { },
                    "repeat": {
                        "mean": 1000,
                        "stdev": 500,
                        "min": 500,
                        "max": 3000
                    }
                }
            }
        ]
    }
